package internal

import (
	"fmt"

	"gitlab.com/gitlab-org/gitlab-shell/client"
	"gitlab.com/gitlab-org/manage/access/pam-gitlab/internal/config"
	"gitlab.com/gitlab-org/manage/access/pam-gitlab/internal/gitlabnet"
)

type Client struct {
	config *config.Config
	client *client.GitlabNetClient
}

type RequestBodyConfig struct {
	KeyId string `json:"key_id"`
}

type RequestBodyCheck struct {
	KeyId      string `json:"key_id"`
	OtpAttempt string `json:"otp_attempt"`
}

type ResponseConfig struct {
	Success         bool `json:"success"`
	TwoFactorEnable bool `json:"two_factor_enable"`
}

type ResponseCheck struct {
	Success bool `json:"success"`
}

func NewClient(config *config.Config) (*Client, error) {
	client, err := gitlabnet.GetClient(config)
	if err != nil {
		return nil, fmt.Errorf("Error creating http client: %v", err)
	}

	return &Client{config: config, client: client}, nil
}

func (c *Client) CheckOTP(gitlabKeyId string, otp string) (bool, error) {
	requestBody := &RequestBodyCheck{KeyId: gitlabKeyId, OtpAttempt: otp}

	response, err := c.client.Post("/two_factor_check", requestBody)
	if err != nil {
		return false, err
	}
	defer response.Body.Close()

	json_response := &ResponseCheck{}
	if err := gitlabnet.ParseJSON(response, json_response); err != nil {
		return false, err
	}

	return json_response.Success, nil
}

func (c *Client) GetConfig(gitlabKeyId string) (bool, error) {
	requestBody := &RequestBodyConfig{KeyId: gitlabKeyId}

	response, err := c.client.Post("/two_factor_config", requestBody)
	if err != nil {
		return false, err
	}
	defer response.Body.Close()

	json_response := &ResponseConfig{}
	if err := gitlabnet.ParseJSON(response, json_response); err != nil {
		return false, err
	}

	return json_response.Success && json_response.TwoFactorEnable, nil
}
